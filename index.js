console.log("Hello World");

// First part of activity

let firstName = "John";
console.log("First Name: " + firstName);
let lastName = "Smith";
console.log("Last Name: " + lastName);
let age = 30;
console.log("Age: " + age);
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log("Hobbies:");
console.log(hobbies);
let workAddress = {
    houseNumber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
}
console.log("Work Address:");
console.log(workAddress);

// Debbuging Part

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

age = 40;
console.log("My current age is: " + age);
	
let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false
}
	
console.log("My Full Profile: ");
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);
